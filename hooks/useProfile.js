import useSWR from "swr";
import Router from 'next/router';

const fetcher = (url) => fetch(url).then((res) => res.json());

function getProfile() {
  const { data: data, error } = useSWR('/api/users/me', fetcher);
  if (data?.missingToken === true) {
    Router.push('/login');
  }
  return { data, error };
}

export default getProfile;