import { createTheme } from '@mui/material/styles';
import { red } from '@mui/material/colors';

// Create a theme instance.
const theme = createTheme({
  palette: {
    primary: {
      main: '#332F2F',
    },
    secondary: {
      main: '#3744CA',
    },
    latinDarkBlue: {
      main: '#262F93',
    },
    success: {
      main: '#4CAF50',
    },
    white: {
      main: '#f9f9f9',
    },
    grey: {
      main: '#ECE9E9',
    },
    error: {
      main: red.A400,
    },
  },
  components: {
    MuiListItemButton: {
      styleOverrides: {
        root: {
          "&.Mui-selected": {
            borderBottom: '5px solid #332F2F',
            backgroundColor: 'transparent',
          }
        }
      }
    }    
  }
});

export default theme;
