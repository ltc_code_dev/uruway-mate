const patch = async function (route, params) {
    const request = await fetch('/api' + route, {
        method: 'PATCH',
        body: JSON.stringify(params),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(function(response) {
        return response.json();
    }).catch(function(error) {
        return error;
    });
    return request;
};

const post = async function (route, params) {
    const request = await fetch('/api' + route, {
        method: 'POST',
        body: JSON.stringify(params),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(function(response) {
        return response.json();
    }).catch(function(error) {
        return error;
    });
    return request;
}

const deleteRequest = async function (route, params = []) {
    const request = await fetch('/api' + route, {
        method: 'DELETE',
        body: JSON.stringify(params),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(function(response) {
        return response.json();
    }).catch(function(error) {
        return error;
    });
    return request;
}

export {patch, post, deleteRequest};