import React, { useState } from 'react';
import Box from '@mui/material/Box';
import { Trans } from '@lingui/macro';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import LoadingButton from '@mui/lab/LoadingButton';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { post } from "../services/codeapi";
import { useSWRConfig } from 'swr';

export default function TeamMemberInvite(props) {
    const [data, setData] = useState({
        user: '',
        role: 'member'
    });
    const [loading, setLoading] = useState(false);
    const { mutate } = useSWRConfig();

    // Handle User input
    const handleUserChange = event => {
        setData({
            ...data,
            ['user']: event.target.value
        });
    }
    // Handle Role select
    const handleRoleChange = event => {
        setData({
            ...data,
            ['role']: event.target.value
        });
    }

    // Handle invite button
    const handleInvite = async event => {
        event.preventDefault();
        setLoading(true);
        const response = await post('/teams/' + props.team.slug + '/users', { user: data.user, role: data.role });
        if (response.ok) {
            setLoading(false);
            await mutate("/api/teams/" + props.team.slug + "/users");
            setData({
                user: '',
                role: 'member'
            });
        }
        else {
            setLoading(false);
        }
    }

    return (<>
        <Grid container spacing={6}>
            <Grid item xs>
                <Box ml={2}>
                    <Box ml={2}>
                        <Typography component="h1" color="inherit">
                            <Trans>Invite Member</Trans>
                        </Typography>
                    </Box>
                    <Box ml={2} mt={4}>
                        <form onSubmit={handleInvite}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                id="user"
                                label={<Trans>Email Address</Trans>}
                                name="user"
                                type="email"
                                autoFocus
                                required
                                value={data.user}
                                onChange={handleUserChange}
                            />
                            <Select
                                labelId="role-selector"
                                id="role-selector"
                                label={<Trans>Role</Trans>}
                                defaultValue="member"
                                value={data.role}
                                onChange={handleRoleChange}
                            >
                                <MenuItem value='member'><Trans>Member</Trans></MenuItem>
                                <MenuItem value='Owner'><Trans>Owner</Trans></MenuItem>
                            </Select>
                            <LoadingButton
                                type="submit"
                                variant="contained"
                                color="primary"
                                loading={loading}
                                loadingPosition="start"
                            >
                                <Trans>Invite</Trans>
                            </LoadingButton>
                        </form>
                    </Box>
                </Box>
            </Grid>
        </Grid>
    </>);
}