import AppBar from '../components/AppBar';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';
import { useRouter } from "next/router";


export default function Layout({ children }) {
    const router = useRouter();
    if (router.pathname != "/login" && router.pathname != "/githubSuccess")
        return (
            <>
                <AppBar />
                <NavBar />
                <main>{children}</main>
                <div className='push'></div>
                <Footer />
            </>
        )
    else
        return (
            <>
                <main>{children}</main>
            </>
        )
}