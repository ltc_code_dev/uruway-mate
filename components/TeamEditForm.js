import React, { useState } from 'react';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import TextareaAutosize from '@mui/base/TextareaAutosize';
import LoadingButton from '@mui/lab/LoadingButton';
import { Trans } from '@lingui/macro';
import { patch } from "../services/codeapi";

export default function TeamEditForm(props) {
    const [data, setData] = useState({
        name: props.team.name,
        slug: props.team.slug,
        bio: props.team.bio
    });
    const [loading, setLoading] = useState(false);

    // Handle name input
    const handleNameChange = event => {
        setData({
            ...data,
            ['name']: event.target.value,
        });
    }

    // Handle slug input
    const handleSlugChange = event => {
        setData({
            ...data,
            ['slug']: event.target.value
        });
    }

    // Handle bio input
    const handleBioChange = event => {
        setData({
            ...data,
            ['bio']: event.target.value
        });
    }

    const handleSave = async event => {
        event.preventDefault();
        setLoading(true);
        const response = await patch('/teams/' + props.team.slug, { name: data.name, slug: data.slug, bio: data.bio });
        if (response.ok) {
            Router.push('/teams');
        }
        else {
            setLoading(false);
        }
    }

    if (!props.team) return <h1>Loading...</h1>

    return (
        <>
            <Grid container spacing={6}>
                <Grid item xs>
                    <Box ml={2}>
                        <form onSubmit={handleSave}>
                            <Typography component="h1" color="inherit">
                                <Trans>Team Manage</Trans>
                            </Typography>
                            <div>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    id="name"
                                    label={<Trans>Team Name</Trans>}
                                    name="name"
                                    type="text"
                                    autoFocus
                                    required
                                    value={data.name}
                                    onChange={handleNameChange}
                                />
                            </div>
                            <div>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    id="slug"
                                    label={<Trans>Slug</Trans>}
                                    name="slug"
                                    type="text"
                                    required
                                    value={data.slug}
                                    onChange={handleSlugChange}
                                />
                            </div>
                            <div>
                                <TextareaAutosize
                                    aria-label="minimum height"
                                    minRows={4}
                                    placeholder="Escribe lo que quieras aquí!"
                                    style={{ width: 200 }}
                                    defaultValue={data.bio}
                                    onChange={handleBioChange}
                                />
                            </div>
                            <LoadingButton
                                type="submit"
                                variant="contained"
                                color="primary"
                                loading={loading}
                            //loadingPosition="start"
                            >
                                <Trans>Save</Trans>
                            </LoadingButton>
                        </form>
                    </Box>
                </Grid>
            </Grid>
        </>
    );
}