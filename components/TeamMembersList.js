import React, { useState, useRef } from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import IconButton from '@mui/material/IconButton';
import { DeleteForever, CheckBoxOutlineBlank, CheckBox } from '@mui/icons-material';
import { Trans } from '@lingui/macro';
import { useGetTeamMembers } from "../hooks/useRequest";
import { useSWRConfig } from 'swr';
import { deleteRequest } from "../services/codeapi";
import ConfirmDialog from './ConfirmDialog';

export default function TeamMembersList(props) {
    const [confirmOpen, setConfirmOpen] = useState(false);
    const elementToDelete = useRef();
    const { mutate } = useSWRConfig();
    let members = [];
    if (props.team) {
        members = useGetTeamMembers("/api/teams/" + props.team.slug + "/users");
    }
    if (!members) return <h1>Loading...</h1>

    const handleDelete = (uuid) => {
        elementToDelete.current = uuid;
        setConfirmOpen(true);
    }

    const handleDeleteAction = async () => {
        // Ejecuto el DELETE contra CodeApi
        const response = await deleteRequest("/teams/" + props.team.slug + "/users", { 'memberUuid': elementToDelete.current });

        // Llamo a mutate para que revalide la lista con la eliminación.
        await mutate("/api/teams/" + props.team.slug + "/users");
    };

    return (<>
        <Box display="flex" justifyContent="left" alignItems="left" m={2}>
            <Box ml={2}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow key="head">
                            <TableCell align="center"><Trans>Name</Trans></TableCell>
                            <TableCell align="center"><Trans>Role</Trans></TableCell>
                            <TableCell align="center"><Trans>Created At</Trans></TableCell>
                            <TableCell align="center"><Trans>Updated At</Trans></TableCell>
                            <TableCell align="center"><Trans>Confirmed</Trans></TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Object.entries(members).map(([key, member]) => {
                            return (
                                <TableRow key={member.uuid} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                    <TableCell align="left">{member.name}</TableCell>
                                    <TableCell align="left">{member.role}</TableCell>
                                    <TableCell align="left">{new Date(member.createdAt).toLocaleString()}</TableCell>
                                    <TableCell align="left">{new Date(member.updatedAt).toLocaleString()}</TableCell>
                                    <TableCell align="left">
                                        {member.confirmed
                                            ? <CheckBox fontSize="small" />
                                            : <CheckBoxOutlineBlank fontSize="small" />
                                        }
                                    </TableCell>
                                    <TableCell align="left">
                                        {member.role == 'member' &&
                                            <IconButton aria-label="delete" onClick={() => handleDelete(member.uuid)}>
                                                <DeleteForever />
                                            </IconButton>
                                        }
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
                <ConfirmDialog
                    title={<Trans>Confirm member removal</Trans>}
                    open={confirmOpen}
                    setOpen={setConfirmOpen}
                    onConfirm={handleDeleteAction}
                >
                    <Trans>Are you sure you want to remove this team member?</Trans>
                </ConfirmDialog>
            </Box>
        </Box>
    </>)
}