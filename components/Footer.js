import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import LangSelector from './LangSelector';

export default function Footer() {

    return (
      <Box 
        sx={{
        width: '100%',
        backgroundColor: 'primary.main',
        color: 'white.main',
        minHeight: '200px',
        padding: '2em',
        position: 'absolute',
        }}
      >
        <List
              sx={{
                display: 'flex',
                flexFlow: 'row wrap',
                maxWidth: '15em',
                backgroundColor: 'primary.secondary'
              }}>
              <ListItemButton>
                <LangSelector />
              </ListItemButton>
        </List>
        <Typography variant="caption" display="block" align='center'>
            LatinCloud CODE - Containers on Demand - { new Date().getFullYear() }
        </Typography>
        <Typography variant="overline" display="block" gutterBottom align='center'>
            Hecho con 💜 en latam
        </Typography>
      </Box>      
    );
  }