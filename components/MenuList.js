import * as React from 'react';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import VerifiedUserIcon from '@mui/icons-material/VerifiedUser';
import DesktopMacIcon from '@mui/icons-material/DesktopMac';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import StorageIcon from '@mui/icons-material/Storage';
import EmailIcon from '@mui/icons-material/Email';
import LayersIcon from '@mui/icons-material/Layers';

export default function MenuList() {
  return (
    <Box sx={{ width: '100%', margin: '1em 0 0 1em',
     maxWidth: 240, bgcolor: 'secondary.main' , color: '#fff',
     borderRadius: 2 }}>
      <nav aria-label="main mailbox folders">
        <List>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <VerifiedUserIcon color="white" />
              </ListItemIcon>
              <ListItemText primary="Dashboard" />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <DesktopMacIcon color="white" />
              </ListItemIcon>
              <ListItemText primary="Proyectos" />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <TrendingUpIcon color="white" />
              </ListItemIcon>
              <ListItemText primary="Estadísticas" />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <StorageIcon color="white" />
              </ListItemIcon>
              <ListItemText primary="Bases de datos" />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <EmailIcon color="white" />
              </ListItemIcon>
              <ListItemText primary="Correos" />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <LayersIcon color="white" />
              </ListItemIcon>
              <ListItemText primary="Dominios" />
            </ListItemButton>
          </ListItem>

        </List>
      </nav>      
    </Box>
  );
}