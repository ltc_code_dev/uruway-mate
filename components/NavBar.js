import React, { useEffect } from 'react';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Box from '@mui/material/Box';
import { Trans } from '@lingui/macro';
import Link from 'next/link';
import { useRouter } from 'next/router'

export default function NavBar() {
  /* const [selectedIndex, setSelectedIndex] = React.useState(1);

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  }; */

  const router = useRouter();
  const firstPath = router.pathname.split('/')[1];

  return (
    <Box
      sx={{
        width: '100%',
        backgroundColor: 'grey.main',
      }}
    >
      <List component="nav" aria-label="menu de navegacion"
        sx={{
          display: 'flex',
          flexFlow: 'row nowrap',
          margin: '.3em',
          padding: '.1em',
          maxWidth: '35em',
        }}
      >
        <Link href="/">
          <ListItemButton
            selected={firstPath === ''}
            //onClick={(event) => handleListItemClick(event, 1)}
          >
            <ListItemText primary={<Trans>Home</Trans>} />
          </ListItemButton>
        </Link>
        <Link href="/">
          <ListItemButton
            selected={firstPath === 'accounts'}
            //onClick={(event) => handleListItemClick(event, 2)}
          >
            <ListItemText primary={<Trans>Accounts</Trans>} />
          </ListItemButton>
        </Link>
        <Link href="/teams">
          <ListItemButton
            selected={firstPath === 'teams'}
            //onClick={(event) => handleListItemClick(event, 3)}
          >
            <ListItemText primary={<Trans>Teams</Trans>} />
          </ListItemButton>
        </Link>
        <Link href="/activity">
          <ListItemButton
            selected={firstPath === 'activity'}
            //onClick={(event) => handleListItemClick(event, 4)}
          >
            <ListItemText primary={<Trans>Activity</Trans>} />
          </ListItemButton>
        </Link>
      </List>
    </Box>
  );
}
