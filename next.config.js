/**
 * @type {import('next').NextConfig}
 */
const nextConfig = {
    i18n: {
        locales: ['en', 'es', 'pseudo'],
        defaultLocale: 'es',
        localeDetection: false,
    }
}

module.exports = nextConfig