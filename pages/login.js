import React, { useState, useEffect, useContext } from "react";
import { useForm } from 'react-hook-form';
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import CloudCircleIcon from "@mui/icons-material/CloudCircle";
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from "@mui/material/Typography";
import LoginIcon from '@mui/icons-material/Login';
import GitHubIcon from '@mui/icons-material/GitHub';
import { store as globalStore } from "../src/globalstore"
import Router from 'next/router';
import { Trans } from '@lingui/macro';
import openPopup from '../src/functions';

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="https://latincloud.com/">
        ltc code
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

export default function login() {
  const [loading, setLoading] = useState(false);
  const [loadingGithub, setLoadingGithub] = useState(false);
  const [successSubmit, setSuccessSubmit] = useState(false);
  const { state } = useContext(globalStore)
  const user = state.user
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    setLoading(true);
    const res = await fetch('/api/users/signin?email=' + data.emailRequired);
    const result = await res.json();
    if (result.ok) {
      setLoading(false);
      setSuccessSubmit(true);
    }
  }

  // Handle Gitgub Login
  const handleGithubLogin = async () => {
    setLoadingGithub(true);
    openPopup('/api/users/github/signin', 'Github Login', 800, 600);
  }

  // Redirect si el usuario esta logueado lo llevo a la raiz, no se si es definitivo pero funciona.
  if (user !== undefined && user.email) {
    Router.push('/');
    return false;
  }

  return (

    <Grid container component="main" sx={{ height: "100vh" }}>
      <CssBaseline />
      <Grid
        item
        xs={false}
        sm={4}
        md={7}
        sx={{
          backgroundImage: "url(/img/background-servers.jpg)",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <Box
          sx={{
            my: 9,
            mx: "auto",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            maxWidth: "20em",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "primary.main" }}>
            <CloudCircleIcon />
          </Avatar>
          {!successSubmit &&
            <Box>
              <Typography component="h1" variant="h5">
                <Trans>Sign in</Trans>
              </Typography>
              <Box component="form"
                noValidate
                onSubmit={handleSubmit(onSubmit)}
                sx={{ mt: 1 }}
              >
                <TextField
                  margin="normal"
                  fullWidth
                  id="email"
                  label={<Trans>Email Address</Trans>}
                  name="email"
                  autoComplete="email"
                  autoFocus
                  {...register("emailRequired", { required: true, pattern: /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ })}
                  disabled={loading}
                />
                {errors.emailRequired &&
                  <Typography component="span" variant="subtitle2">
                    <Trans>Please enter a valid Email Address</Trans>
                  </Typography>
                }
                <LoadingButton
                  color="primary"
                  type="submit"
                  fullWidth
                  loading={loading}
                  loadingPosition="start"
                  sx={{ mt: 3, mb: 2 }}
                  startIcon={<LoginIcon />}
                  variant="contained"
                >
                  <Trans>Email Link</Trans>
                </LoadingButton>
              </Box>
              <LoadingButton
                color="primary"
                type="submit"
                fullWidth
                loading={loadingGithub}
                loadingPosition="start"
                onClick={handleGithubLogin}
                sx={{ mt: 3, mb: 2 }}
                startIcon={<GitHubIcon />}
                variant="contained"
              >
                <Trans>Github Login</Trans>
              </LoadingButton>
              <Copyright sx={{ mt: 5 }} />

            </Box>
          }
          {successSubmit && <Box
            sx={{ mt: 1 }}
          >
            <Typography component="h1" variant="h5">
              <Trans>You'll receive a verification email to continue Signing In.</Trans>
            </Typography>
          </Box>
          }
        </Box>
      </Grid>
    </Grid>
  );
}
