import React from 'react';
import PropTypes from "prop-types";
import Head from "next/head";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { CacheProvider } from "@emotion/react";
import theme from "../src/theme";
import '../src/globalStyle.css';
import createEmotionCache from "../src/createEmotionCache";
import { store, StateProvider as GlobalStateProvider } from '../src/globalstore.js';
import { StateProvider as ApiStateProvider } from '../src/apistore.js';
import { APIWrap, fetcher } from "../src/CodeAPI.js";
import CheckAuth from '../components/CheckAuth';
import { SWRConfig } from 'swr';
import { I18nProvider } from '@lingui/react';
import { i18n } from '@lingui/core';
import { messages as messagesEn } from '../src/translations/locales/en/messages';
import { messages as messagesEs } from '../src/translations/locales/es/messages';
import { en, es } from "make-plural/plurals";
import Loading from '../components/Loading';
import Layout from '../components/Layout';

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

// Loading 
i18n.load({
  en: messagesEn,
  es: messagesEs,
});

// Inicializo los plurls
i18n.loadLocaleData("en", { plurals: en })
i18n.loadLocaleData("es", { plurals: es })
// Activo el i18n
i18n.activate('es');

export default function MyApp(props) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  const getLayout = Component.getLayout || ((page) => page);

  return getLayout(
    <CacheProvider value={emotionCache}>
      <GlobalStateProvider>
        <ApiStateProvider>
          <Head>
            <title>CoDe Console</title>
            <meta name="viewport" content="initial-scale=1, width=device-width" />
          </Head>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <SWRConfig value={{
              fetcher: fetcher,
              use: [APIWrap]
            }}>
              <I18nProvider i18n={i18n}>
                <CheckAuth>
                  <Layout>
                    <Component {...pageProps} />
                  </Layout>
                  <Loading />
                </CheckAuth>
              </I18nProvider>
            </SWRConfig>
          </ThemeProvider>
        </ApiStateProvider>
      </GlobalStateProvider>
    </CacheProvider>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  emotionCache: PropTypes.object,
  pageProps: PropTypes.object.isRequired,
};
