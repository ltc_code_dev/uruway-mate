import React, { useContext, useEffect } from 'react';
import { Trans } from '@lingui/macro';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import ProTip from '../src/ProTip';
import Link from 'next/link';
import Copyright from '../src/Copyright';
import { store as globalStore } from "../src/globalstore.js";
import Layout from "../components/Layout";

export default function index() {
  const { state } = useContext(globalStore)
  const user = state.user

  return (
    <>
      <Container maxWidth="sm">
        <Box sx={{ my: 4 }}>
          <Typography variant="h4" component="h1" gutterBottom>
            {user && user.email &&
              <Trans>Hello {user.email}</Trans>
            }
          </Typography>
          {!user && <Link href="/login" color="secondary">
            Go to login page
          </Link>}
          <ProTip />
          <Copyright />
        </Box>
      </Container>
    </>
  );
}