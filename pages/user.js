import * as React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import ProTip from '../src/ProTip';
import Link from '../src/Link';
import Copyright from '../src/Copyright';
import getUser from "../src/CodeAPI";
import SignInSide from "../pages/SignInSide";
import Router from 'next/router';

export default function Index() {
  const { user, isLoading, isError } = getUser();


  return (
    <Container maxWidth="sm">
      <Box sx={{ my: 4 }}>
        <Typography variant="h4" component="h1" gutterBottom>
          {user ? 'Hola '+user.email : null}
        </Typography>
        <button type="button" onClick={() => router.push('/SigninSide')} color="secondary">
          Go to login page
        </button>
        <ProTip />
        <Copyright />
      </Box>
    </Container>
  );
}
