import React, { useState, useEffect } from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';
import ProTip from '../src/ProTip';
import Link from '../src/Link';
import AppBar from '../components/AppBar.js';
import MenuList from '../components/MenuList';
import Copyright from '../src/Copyright';
import { useStoreState } from 'easy-peasy';
import { useAuthSession } from '../hooks/user.js';


export default function Dashboard() {
  const user = useAuthSession();
  if (!user?.email) return null;
console.log(user)
  return (
    <>
    <AppBar />
    <MenuList />
    <Container maxWidth="sm">      
      <Box sx={{ my: 4 }}>
        <Typography variant="h4" component="h2" gutterBottom>
          {user && user.email ? 'Hola '+user.email : <Skeleton />}
        </Typography>
        <Link href="/SignIn" color="secondary">
          Go to login page
        </Link>
        <ProTip />
        <Copyright />
      </Box>
    </Container>
    </>
  );
}

Dashboard.auth = true
