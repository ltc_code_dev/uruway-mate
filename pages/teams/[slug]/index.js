import React, { useState } from 'react';
import { useRouter } from 'next/router'
import { useGetTeam } from "../../../hooks/useRequest";
import TeamEditForm from '../../../components/TeamEditForm';

export default function index() {
    const router = useRouter();
    const { slug } = router.query;
    const team = useGetTeam("/api/teams/" + slug);

    if (!team) return <h1>Loading...</h1>

    return (<TeamEditForm team={team} />);
}