import React, { useState, useRef } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import Button from "@mui/material/Button";
import { useGetTeams } from "../../hooks/useRequest";
import { deleteRequest } from "../../services/codeapi";
import Container from "@mui/material/Container";
import { Trans } from "@lingui/macro";
import { useSWRConfig } from "swr";
import Link from 'next/link';

export default function index() {
  const { teams, error } = useGetTeams("/api/users/teams");
  const [confirmOpen, setConfirmOpen] = useState(false);
  const elementToDelete = useRef();
  const { mutate } = useSWRConfig();

  if (error) return <h1>Something went wrong!</h1>;
  if (!teams) return <h1>Loading...</h1>;

  const handleDelete = (uuid) => {
    elementToDelete.current = uuid;
    setConfirmOpen(true);
  };

  const handleDeleteAction = async () => {
    // Ejecuto el DELETE contra CodeApi
    const response = await deleteRequest("/teams/" + elementToDelete.current);

    // Llamo a mutate para que revalide la lista con la eliminación.
    await mutate("/api/users/teams");
  };

  return (
    <>
      <Container>
        <Box
          sx={{ display: "flex", justifyContent: "space-evenly", m: "2rem" }}
        >
          <Box>
            <Typography variant="h1" component="h2" sx={{ fontSize: "2rem" }}>
              <Trans>Teams</Trans>
            </Typography>
          </Box>
          <Box>
            <Link href="/teams/create">
              <Button variant="contained" color="primary">
                <Trans>New Team</Trans>
              </Button>
            </Link>
          </Box>
        </Box>
        <Typography sx={{ display: "flex", justifyContent: "center" }}>
          <Trans>Organize your teams by adding or deleting members</Trans>
        </Typography>
        {Object.entries(teams).map(([key, team]) => {
          return (
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                my: "1em",
                mx: "auto",
                p: ".5rem",
                maxWidth: "35rem",
                border: "1px solid #ddd",
                borderRadius: "5px",
              }}
            >
              <Typography variant="h2" component="h2" sx={{ fontSize: "1rem" }}>
                {team.name}
              </Typography>
              <Box sx={{ display: "flex", alignItems: "center" }}>
                <Link href={{ pathname: '/teams/' + team.slug + '/members' }}>
                  <Button variant="outlined" color="primary" size="small" sx={{ m: "10px" }}>
                    <Trans>View members</Trans>
                  </Button>
                </Link>
                <Link href={{ pathname: '/teams/' + team.slug }}>
                  <Button variant="outlined" color="primary" size="small" sx={{ m: "10px" }}>
                    <Trans>Settings</Trans>
                  </Button>
                </Link>
                <MoreVertIcon fontSize="small" />
              </Box>
            </Box>
          );
        })}
      </Container>

      {/* <Box display="flex" justifyContent="right" alignItems="right" m={2}>
                <Box ml={2}>
                    <Link href="/teams/create">
                        <Button
                            variant="contained"
                            color="primary"
                        >
                            <Trans>New Team</Trans>
                        </Button>
                    </Link>
                </Box>
            </Box>
            <Box display="flex" justifyContent="left" alignItems="left" m={2}>
                <Box ml={2}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                            <TableRow key="head">
                                <TableCell align="center"><Trans>Team</Trans></TableCell>
                                <TableCell align="center"><Trans>Slug</Trans></TableCell>
                                <TableCell align="center"><Trans>Role</Trans></TableCell>
                                <TableCell align="center"><Trans>Created At</Trans></TableCell>
                                <TableCell align="center"><Trans>Members</Trans></TableCell>
                                <TableCell align="center"><Trans>UUID</Trans></TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {Object.entries(teams).map(([key, team]) => {
                                return (
                                    <TableRow
                                        key={team.uuid}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell align="right">{team.name}</TableCell>
                                        <TableCell align="right">{team.slug}</TableCell>
                                        <TableCell align="right">{team.role}</TableCell>
                                        <TableCell align="right">{new Date(team.createdAt).toLocaleString()}</TableCell>
                                        <TableCell align="center">{team.members}</TableCell>
                                        <TableCell align="center">{team.uuid}</TableCell>
                                        <TableCell align="center">
                                            {team.role == 'owner' &&
                                                <React.Fragment>
                                                    <React.Fragment>
                                                    <Link href={{ pathname: '/teams/' + team.slug }}>
                                                        <IconButton aria-label="settings">
                                                            <Settings />
                                                        </IconButton>
                                                    </Link>
                                                    </React.Fragment>
                                                    <IconButton aria-label="delete" onClick={() => handleDelete(team.uuid)}>
                                                        <DeleteForever />
                                                    </IconButton>
                                                </React.Fragment>
                                            }
                                            <Link href={{ pathname: '/teams/' + team.slug + '/members' }}>
                                                <IconButton aria-label="People">
                                                    <People />
                                                </IconButton>
                                            </Link>
                                        </TableCell>
                                    </TableRow>
                                )
                            }
                            )}
                        </TableBody>
                    </Table>
                    <ConfirmDialog
                        title={<Trans>Confirm Team Delete</Trans>}
                        open={confirmOpen}
                        setOpen={setConfirmOpen}
                        onConfirm={handleDeleteAction}
                    >
                        <Trans>Are you sure you want to delete this team?</Trans>
                    </ConfirmDialog>
                </Box>
            </Box> */}
    </>
  );
}
