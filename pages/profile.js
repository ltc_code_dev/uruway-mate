import React, { useState, useEffect, useContext } from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import LoadingButton from '@mui/lab/LoadingButton';
import GitHubIcon from '@mui/icons-material/GitHub';
import Box from '@mui/material/Box';
import { Trans } from '@lingui/macro';
import patch from "../services/codeapi";
import { store as globalStore } from "../src/globalstore.js";
import { useSWRConfig } from 'swr';
import openPopup from '../src/functions';

export default function profile() {
    // Starts data State and loading button state
    const [data, setData] = useState({
        name: '',
        email: '',
        githubConnected: '',
    });
    const [loadingGithub, setLoadingGithub] = useState();

    // Handle user input on textbox
    const handleInputChange = event => {
        setData({
            ...data,
            [event.target.name]: event.target.value
        });
    };

    // Load data with global state
    const { state, dispatch } = useContext(globalStore);
    const { mutate } = useSWRConfig();
    useEffect(() => {
        if (state.user && state.user.name) {
            setData({
                ...data,
                name: state.user.name,
                email: state.user.email,
                githubConnected: state.user.githubConnected
            });
        }
    }, []);

    const handleGithubDisconnect = async () => {
        setLoadingGithub(true);
        const res = await fetch('/api/users/github/disconnect');
        const result = await res.json();
        if (result.ok == 'disconnected') {
            setLoadingGithub(false);
            setData({
                ...data,
                githubConnected: 0
            });
        }
    }

    const handleGithubConnect = async () => {
        openPopup('/api/users/github/connect', 'Github Login', 800, 600);
        setLoadingGithub(true);
        const interval = setInterval(() => {
            mutate('/api/users/me');
            if(state.user.githubConnected == 1)
            {
                clearInterval(interval);
                setLoadingGithub(false);
                setData({
                    ...data,
                    githubConnected: 1
                });
            }
        }, 1000);
    }

    async function handleSave() {
        // Ejecuto mutate para actualizar el cache sin verificar contra el API
        mutate('/api/users/me', { ...data, name: data.name, email: data.email }, false);

        // Llamo PATCH para actualizar la información
        await patch('/users/me', { name: data.name, email: data.email });

        //dispatch({ type: 'updateUser', payload: data }); No sería necesario

        // Ejecuto nuevamente un mutate para validar si la información fue actualizada y sino vuelvo al estado anterior.
        mutate('/api/users/me');
    }

    return (
        <>
            <Container maxWidth="sm">
                <Box display="flex" justifyContent="center" alignItems="center" m={2}>
                    <Box ml={2}>
                        <Typography component="h2" color="inherit">
                            Datos del usuario
                        </Typography>
                    </Box>
                </Box>
                <Grid container spacing={6}>
                    <Grid item xs>
                        <Box pt={4}>
                            <Typography component="h4" color="inherit">
                                Datos del usuario
                            </Typography>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                id="name"
                                label={<Trans>Name</Trans>}
                                name="name"
                                type="text"
                                autoFocus
                                value={data.name}
                                onChange={handleInputChange}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                id="email"
                                label={<Trans>Email</Trans>}
                                name="email"
                                type="text"
                                value={data.email}
                                onChange={handleInputChange}
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={handleSave}
                            >
                                <Trans>Save</Trans>
                            </Button>
                            {data.githubConnected == 1 &&
                                <LoadingButton
                                    color="primary"
                                    type="submit"
                                    fullWidth
                                    loading={loadingGithub}
                                    loadingPosition="start"
                                    onClick={handleGithubDisconnect}
                                    sx={{ mt: 3, mb: 2 }}
                                    startIcon={<GitHubIcon />}
                                    variant="contained"
                                >
                                    <Trans>Disconnect from Github</Trans>
                                </LoadingButton>
                            }
                            {!data.githubConnected &&
                                <LoadingButton
                                    color="primary"
                                    type="submit"
                                    fullWidth
                                    loading={loadingGithub}
                                    loadingPosition="start"
                                    onClick={handleGithubConnect}
                                    sx={{ mt: 3, mb: 2 }}
                                    startIcon={<GitHubIcon />}
                                    variant="contained"
                                >
                                    <Trans>Connect with Github</Trans>
                                </LoadingButton>
                            }
                        </Box>
                    </Grid>
                </Grid>
            </Container>
        </>
    );
}